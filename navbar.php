<?php
    SESSION_START();
    include('config.php');
    $conexion = mysqli_connect($db_addrs, $db_user, $db_passwd, $db_name) or
            die("Problemas con la conexión");
    if(isset($_SESSION["login"]) && isset($_SESSION["user"]) && isset($_SESSION["passwd"])){
        $resultado = mysqli_query($conexion, "SELECT * FROM usuarios WHERE usuario=\"$_SESSION[user]\";");
        $resultado = $resultado->fetch_array();
        if($resultado === null){
            $error = true;
        }else{
            if($resultado["password"] == $_SESSION["passwd"]){

            }else{
                header("location: ./index.php");
                die;
            }
        }

    }else{
        header("location: ./index.php");
        die;
    }
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light justify-content-between">
    <a class="navbar-brand" href="index.php">Agenda</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="./listadoContactos.php">Contactos</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="./agregar.php">Crear contacto</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="./borrar.php">Borrar contacto</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="./modiconsul.php">Modificar contacto</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="./logout.php">Cerrar sesión</a>
            </li>
        </ul>

    </div>
</nav>
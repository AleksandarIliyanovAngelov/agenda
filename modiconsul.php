
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Consultar</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</head>
<body>
    <?php
        include('navbar.php');
    ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Modificar contacto</h1>
                <?php 
                    if(isset($_POST["apellido"]) && isset($_POST["buscar"])){
                        if (!empty($_POST)){
                            $_SESSION['apellido'] = $_POST['apellido'];
                            $_SESSION['buscar'] = $_POST['buscar'];
                        }
            
                        if (!$_SESSION['apellido']) {
                            echo "<p class='alert alert-warning'>Introduce el apellido</p>";
                        }
                    }
                ?>
                <form action="modiconsul.php" method="post">
                    <div class="form-group">
                        <label>Buscar por apellido:</label>
                        <input type="text" class="form-control" name="apellido"></input><br><br>
                    </div>
                    
                    <input type="submit" class="btn btn-success" name="buscar" value="Buscar">
                </form>

                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellidos</th>
                        <th scope="col">Dirección</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Teléfono</th>
                        <th scope="col">Editar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        if(isset($_SESSION['buscar']) && isset($_SESSION['apellido'])){
                            if ($_SESSION['buscar'] && $_SESSION['apellido']) {
                                if ($_SESSION['buscar'] == 'Buscar'){

                                    $apellido = $_SESSION['apellido'];
                                    $query = "select nombre, apellidos, domicilio, email, telefono from contactos WHERE apellidos LIKE '%$apellido%'";
                                    $resultado = mysqli_query($conexion, $query);

                                    $resultado = $resultado->fetch_all();
                                    $longitud = count($resultado);
                                
                                    if ($longitud>0){
                                            for ($i = 0; $i < $longitud; $i++){ 
                                                $r = "<tr>";
                                                foreach ($resultado[$i] as $contacto) {
                                                    $r .= "<td>".$contacto."</td>";
                                                }
                                                $r .= "<td><form action='editar.php' method='post'><input type='hidden' name='tlf' value='$contacto'><input type='submit' value='editar' class='btn btn-success'></form></td></tr>";
                                                echo $r;
                                            }
                                    }else{
                                        echo "<tr><td colspan='6'>No hay ningún contacto con ese apellido</td></tr>";
                                    }
                                        
                                }
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
   
</body>
</html>
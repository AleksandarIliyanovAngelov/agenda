<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

    </head>
    <body>
        <?php
            require('./navbar.php');
        ?>
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-4 p-3 border mt-6">
                    <h1>Agregar contacto a la agenda.</h1><br>
                    <form action="" method="post">
                        <div class="form-group mb-2">
                            <label>Nombre: </label>
                            <input type="text" name="nombre" class="form-control">
                        </div>
                        <div class="form-group mb-2">
                            <label>Apellidos: </label>
                            <input type="text" name="apellidos" class="form-control">
                        </div>
                        <div class="form-group mb-2">
                            <label>Domicilio: </label>
                            <input type="text" name="domicilio" class="form-control">
                        </div>
                        <div class="form-group mb-2">
                            <label>Email: </label>
                            <input type="text" name="email" class="form-control">
                        </div>
                        <div class="form-group mb-2">
                            <label>Telefono: </label>
                            <input type="text" name="telefono" class="form-control">
                        </div>
                        <input type="submit" value="Agregar contacto" name="submit" class="btn btn-success">
                    </form>
                </div>
            </div>
        </div>

        <?php

            if(isset($_POST["submit"])){
                $conexion = mysqli_connect("localhost", "root", "", "agendarpro") or
                die("Problemas con la conexión");

                $sql = "INSERT INTO contactos (nombre,apellidos,domicilio,email,telefono) 
                VALUES ('$_POST[nombre]','$_POST[apellidos]','$_POST[domicilio]','$_POST[email]','$_POST[telefono]')";

                $insert=mysqli_query($conexion, $sql )
                or die("Problemas en el select ".mysqli_error($conexion));

                mysqli_close($conexion);
            }

        ?>

    </body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menú de la Agenda</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover({
                placement : 'top',
                trigger : 'hover'
            });
        });
    </script>
</head>
<body>
    <?php include("./navbar.php"); ?>
    <div class="container">
        <div class="row d-flex justify-content-center p-3 mt-6">
        <h1>Opciones</h1>
            <div class="col-lg-12 p-3 mt-6">
                <a href="agregar.php" data-toggle="popover" title="Agre un contacto a la agenda" class="btn btn-success">Agregar</a>
                <a href="borrar.php" data-toggle="popover" title="Borra un contacto de la agenda" class="btn btn-success">Borrar</a>
                <a href="listadoContactos.php" data-toggle="popover" title="Muestra todo tus contactos" class="btn btn-success">Listar contactos</a>
                <a href="modiconsul.php" data-toggle="popover" title="Modifica la informacion de un contacto" class="btn btn-success">Modificar contacto</a>
            </div>
        </div>
    </div>
    
</body>
</html>
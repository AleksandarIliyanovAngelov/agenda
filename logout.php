<?php
    session_start();

    if(isset($_SESSION["login"]) && isset($_SESSION["user"]) && isset($_SESSION["passwd"])){
        unset($_SESSION["login"]);
        unset($_SESSION["user"]);
        unset($_SESSION["passwd"]);
    }

    session_destroy();
    header("location: ./index.php");
    die();
?>
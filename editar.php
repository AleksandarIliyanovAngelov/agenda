<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <title>Editar</title>
</head>
<body>
    <?php
        include('navbar.php');
    ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Editar contacto</h1>
                <form action='editar2.php' method='post'>
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Dirección</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Teléfono</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                            $tlf = $_POST['tlf'];

                            $query = "select nombre, apellidos, domicilio, email, telefono from contactos WHERE telefono = '$tlf'";
                            $resultado = mysqli_query($conexion, $query);

                            $resultado = $resultado->fetch_all();
                            $longitud = count($resultado);

                            if ($longitud>0){
                                $_SESSION['tlf'] = $tlf;
                                for ($i = 0; $i < $longitud; $i++){ 
                                    echo "<tr>";
                                    foreach ($resultado[$i] as $contacto) {
                                        echo "<td>",$contacto,"</td>";
                                    }
                                    echo "</tr>";
                                }
                                echo "<tr><td><input type='text' name ='nombre'></td><td><input type='text' name ='apellidos'></td><td><input type='text' name ='domicilio'></td><td><input type='text' name ='email'><td><input type='text' name ='telefono'></td></td></tr>";
                            }
                        ?>
                        </tbody>
                    </table>
                    <input type='submit' value='Confirmar'>
                </form>
            </div>
        </div>
    </div>

</body>
</html>

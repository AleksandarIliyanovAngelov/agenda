<?php
    SESSION_START();
    require("./config.php");

    $conexion = mysqli_connect($db_addrs, $db_user, $db_passwd, $db_name);
    $error = false;
    
    if(isset($_GET["error"])){
        $error = true;
    }

    if(isset($_SESSION["login"]) && isset($_SESSION["user"]) && isset($_SESSION["passwd"])){
        $resultado = mysqli_query($conexion, "SELECT * FROM usuarios WHERE usuario=\"$_SESSION[user]\";");
        $resultado = $resultado->fetch_array();
        if($resultado === null){
            $error = true;
        }else{
            if($resultado["password"] == $_SESSION["passwd"]){
                header("location: ./menuagenda.php");
                die;
            }else{
                $error = true;
            }
        }

    }else if(isset($_POST["user"]) && isset($_POST["passwd"])){

        $resultado = mysqli_query($conexion, "SELECT * FROM usuarios WHERE usuario=\"$_POST[user]\";");
        $resultado = $resultado->fetch_array();
        if($resultado === null){
            $error = true;
        }else{
            if($resultado["password"] == $_POST["passwd"]){
                $_SESSION["login"] = true;
                $_SESSION["user"] = $_POST["user"];
                $_SESSION["passwd"] = $_POST["passwd"];
                header("location: ./menuagenda.php");
                die;
            }else{
                $error = true;
            }
        }

    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio de sesión</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</head>
<body>
    <!--TEST-->
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-lg-4 p-3 border mt-6">
                <form action="./index.php" method="post">
                    <?php
                        if($error){
                            echo '<div class="alert alert-danger" role="alert">Usuario o contraseña incorrecto.</div>';
                        }
                    ?>
                    <div class="form-group mb-2">
                        <label>Usuario: </label>
                        <input type="text" name="user" class="form-control">
                    </div>
                    <div class="form-group mb-2">
                        <label>Contraseña: </label>
                        <input type="password" name="passwd" class="form-control">
                    </div>
                    <input type="submit" value="Iniciar sesión" class="btn btn-success">
                </form>
            </div>
        </div>
    </div>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Borrar contactos</title>
    <style>
        table{
            border: 1px solid black;
            width: 400px;
        }
        td{
            border: 1px solid black;
            width: 200px;
            height: 80px;
        }
    </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</head>
<body>

    <?php
        require('./navbar.php');
    ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <h1>Lista de Contactos</h1>
            <form action="borrarContacto.php" method="post">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellidos</th>
                        <th scope="col">Dirección</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Teléfono</th>
                        <th scope="col">Borrar?</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        include('config.php');

                        $conexion = mysqli_connect($db_addrs, $db_user, $db_passwd, $db_name) or
                        die("Problemas con la conexión");

                        $sql = "SELECT * FROM contactos ORDER BY nombre";

                        $consulta = mysqli_query($conexion, $sql)
                        or die("Problemas en el select ".mysqli_error($conexion));
                        
                        while ($row=$consulta->fetch_row()) {
                            $r = "<tr>";
                            $r .= '<td>'.$row[1].'</td>';
                            $r .= '<td>'.$row[2].'</td>';
                            $r .= '<td>'.$row[3].'</td>';
                            $r .= '<td>'.$row[4].'</td>';
                            $r .= '<td>'.$row[5].'</td>';
                            $r .= '<td><input type="checkbox" name="contacto[]" value='.$row[0].'></td>';
                            $r .= "</tr>";
                            echo $r;
                        }
                        mysqli_close($conexion);
                    ?>
                    </tbody>
                </table>
                <input type="submit" value="Borrar" class="btn btn-warning">
            </form>
        </div>
    </div>
</body>
</html>